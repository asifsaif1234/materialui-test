import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';


const useStyles = makeStyles((theme) => ({
  appBar: {
    backgroundColor: "#fff"
  } ,
  toolbarButtons: {
    marginLeft: 'auto',
  },
  links: {
    textDecoration: 'none',
  },
}));


function Header() {
  const classes = useStyles();
  return (
    <div>
      <AppBar className={classes.appBar} position="static">
        <Toolbar>
          <Button>
            <Typography variant="h6" color="primary" >
              <Link className={classes.links} to="/">Matrial UI</Link>
            </Typography>
          </Button>
          <div className={classes.toolbarButtons}>
            <Button color="primary" className={classes.links}><Link className={classes.links} to="/login">Login</Link></Button>
            <Button color="primary"><Link className={classes.links} to="/signup">Sign Up</Link></Button>
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}

export default Header;
